<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 26.12.16
 * Time: 18:13
 */

namespace app\commands;

use GearmanClient;
use yii\console\Controller;
use GearmanWorker;
use yii\helpers\Json;

use app\components\daemon\JobDaemon;
use app\components\combinations\CombinationCombine;
use app\models\redis\Mail;
use Faker;


/**
 * Класс ConsoleApplication для Cron
 */
class MailController extends Controller
{
    public $log_path;

    public function init()
    {
        $this->log_path = \Yii::getAlias('@app/runtime/pseudo_mail.log');
        $file_content = "Pseudo_mail_logged" . PHP_EOL;
        if (!file_exists($this->log_path)) {
            file_put_contents($this->log_path, $file_content);
        }
    }

    /**
     * Демон для отправки почты. Дописывает в начало файла JSON, для примера
     */

    public function actionMailWorker()
    {

        $worker = new GearmanWorker();
        $worker->addServer();

        $worker->addFunction('pseudoSendMail', function ($job) {
            sleep(3); // На мой взгляд максимальная задержка exim;
            /**
             * @var \GearmanJob $job
             */
            $workload = $job->workload();
            $data = Json::decode($workload);

            $letter = Mail::findOne($data['id']);
            $letter->setAttributes($data);
            $letter->stat = Mail::STATE_SENDED;
            exec("sed -i -e '1 s/^/$workload\n/;' $this->log_path");
            $letter->save(false);
        });

        while (1) {
            $worker->work();
            if ($worker->returnCode() != GEARMAN_SUCCESS) break;
        }

    }

    /**
     * Демон для учёта почты. выбирает из REDIS не отправленные письма.
     */

    public function actionClientSend()
    {
        $client = new GearmanClient();
        $client->addServer('127.0.0.1', '4730');
        
        while (true) {
            $letters = Mail::find()
                ->where(['state'=>Mail::STATE_WAITING])
                ->all();
            foreach ($letters as $letter) {
                $client->doBackground('pseudoSendMail', Json::encode($letter->attributes));
            }
            
            sleep(1);
        }
        
    }

    /**
     * Демон для учёта почты. выбирает из REDIS не отправленные письма на переотправку.
     */

    public function actionClientResend()
    {
        $client = new GearmanClient();
        $client->addServer('127.0.0.1', '4730');
        
        while (true) {
            $letters = Mail::find()
                ->where(['state'=>Mail::STATE_ERRORED])
                ->all();
            foreach ($letters as $letter) {
                $client->doBackground('pseudoSendMail', Json::encode($letter->attributes));
            }
            sleep(1);     
        }


    }

    /**
     * Демон для генерации почты. Генерирует тестовые письма в REDIS. Аргументы errors - Кол-во вероятных ошибок, count - количество писем за раз, maxSenders - кол-во потоков
     */

    public function actionSenders($errors = 2, $count = 8, $maxSenders = 5)
    {
        $faker = Faker\Factory::create();
        $daemon = new JobDaemon();
        $daemon->maxProcesses = $maxSenders;
        $combination = CombinationCombine::getFirstIntCombination($errors, $count);
        $combination = str_split(str_pad(decbin($combination), $count, '0', STR_PAD_LEFT));
        $daemon->run(function ($jobID) use ($faker, $combination){
            shuffle($combination);
            foreach ($combination as $err) {
                $model = new Mail();
                $model->setAttributes([
                    'to_email' => $faker->email,
                    'from_id' => $jobID,
                    'body' => $faker->text(),
                    'state' => ($err == 1)? Mail::STATE_ERRORED : Mail::STATE_WAITING,
                    'created_at' => time(),
                    'updated_at' => time(),
                    'delay_sec' => 0
                ]);
                $model->save(false);
            }
        });
    }
}