<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 25.12.16
 * Time: 15:23
 */

namespace app\commands;

use app\models\Student;
use app\models\StudentLike;
use yii\base\Exception;
use yii\console\Controller;
use Faker\Factory;
use yii\db\Expression;

/**
 * Заполняет базу студентами и лайками Все парамеры необязательны.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PopulateController extends Controller
{
    /**
     * 1-вый аргумент $count количество студентов. default 10
     * @param integer $count
     * @throws Exception
     */
    public function actionStudents($count = 10)
    {
        if (self::createStudents($count)) 
            print "Done!";
    }
    
    public function actionLikes($count = 1) {
        if (self::createLikes($count))
            print "Done!";
    }

    public static function createStudents($count) {
        $faker = Factory::create();

        $stud_model = new Student();
        $rows = [];
        for ($i = 1; $i <= $count; $i ++) {
            $stud_model->setAttributes([
                'created_at' => time(),
                'updated_at' => time(),
                'full_name' => $faker->name,
                'grade' => round(10 / rand(3, 5), 2),
            ]);
            $rows[] = $stud_model->attributes;
        }

        $transaction = \Yii::$app->db->beginTransaction();
        try {

            $query = \Yii::$app->db
                ->createCommand()
                ->batchInsert($stud_model::tableName(),array_keys($stud_model->attributes), $rows)
                ->execute();
            if ($query)
                $transaction->commit();
                return true;
            

        } catch(Exception $e) {
            $transaction->rollBack();
            var_dump($e);
            return false;
        }

    }


    public static function createLikes($count) {
        $like_model = new StudentLike();
        $rows = [];
        for ($i = 1; $i <= $count; $i ++) {
            $like_model->setAttributes(self::likeGenerate());
            $rows[] = $like_model->attributes;
        }

        $transaction = \Yii::$app->db->beginTransaction();
        
        try {

            $query = \Yii::$app->db
                ->createCommand()
                ->batchInsert($like_model::tableName(),array_keys($like_model->attributes), $rows)
                ->execute();
            if ($query)
                $transaction->commit();
                return true;
            
        } catch(Exception $e) {
            $transaction->rollBack();
            var_dump($e);
            return false;
        }

    }

    /**
     * @return array
     */
    public static function likeGenerate() {
        $like_rand = Student::find()
            ->select(new Expression('id'))
            ->orderBy(new Expression('RAND()'))
            ->one();
        
        $liked_rand = Student::find()
            ->select(new Expression('id'))
            ->orderBy(new Expression('RAND()'))
            ->one();

        $exists = StudentLike::find()
            ->where(['student_like'=>$like_rand->id, 'student_liked' => $liked_rand->id])
            ->exists();
        
        if ($exists || ($like_rand->id == $liked_rand->id)) 
            self::likeGenerate();
        
        
        return [
            'student_like' => $like_rand->id,
            'student_liked' => $liked_rand->id,
            'created_at' => time(),
            'updated_at' => time(),
        ];
    }


}
