<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Student;
use app\models\StudentLike;
use yii\console\Controller;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Console;

/**
 * Результаты выполнения задания №1.
 */
class StudentsController extends Controller
{
    /**
     * Задание А
     */

    public function actionQueryA()
    {
        $connection = \Yii::$app->getDb();
        $command = $connection->createCommand(
            "SELECT S.id, S.full_name, count(L.id) AS lcnt , S.grade
              FROM student AS S
              INNER JOIN student_like as L ON S.id = L.student_like
              GROUP BY S.id
              HAVING lcnt > 1
              ;"
        );
        $command->bindParam(':std:id', $id);
        $result = $command->queryAll();
        foreach ($result as $row) {
            print $row['id'] . " " . $row['full_name'] . " " . $row['lcnt'] . " " . $row['grade'] . PHP_EOL;
        }
    }

    /**
     * Задание В
     * @param $id
     */

    public function actionQueryC()
    {
        $connection = \Yii::$app->getDb();
        $command = $connection->createCommand(
            "SELECT student.id, student.full_name, student.grade, count(L1.student_liked) AS liked_cnt, count(L2.student_like) AS likes_cnt
              FROM student
              LEFT JOIN student_like AS L1 ON L1.student_liked=student.id
              LEFT JOIN student_like AS L2 ON L2.student_like=student.id
              GROUP BY student.id HAVING liked_cnt=0 AND likes_cnt=0"
        );
        $command->bindParam(':std:id', $id);
        $result = $command->queryAll();
        foreach ($result as $row) {
            print $row['id'] . " " . $row['full_name'] . " " . $row['grade'] . " " . $row['liked_cnt'] . " " . $row['likes_cnt'] . PHP_EOL;
        }
    }


    public function actionQueryB()
    {
        $connection = \Yii::$app->getDb();
        $command = $connection->createCommand(
            "SELECT A.id, A.full_name, A.grade
            FROM student A, student B, student_like L
            WHERE A.id=L.student_like
            and B.id=L.student_liked
            and B.id NOT IN (select student_like FROM student_like)"
        );
        $command->bindParam(':std:id', $id);
        $result = $command->queryAll();
        foreach ($result as $row) {
            print $row['id'] . " " . $row['full_name'] . " " . $row['grade']  . PHP_EOL;
        }
    }


}
