<?php

use yii\db\Migration;
use yii\db\mysql\Schema;
class m161225_123908_add_table_students extends Migration
{
    public function up()
    {
        $this->createTable('student', [
            'id' => Schema::TYPE_PK,
            'full_name' => Schema::TYPE_STRING . ' NOT NULL',
            'grade' => Schema::TYPE_FLOAT. ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
    }

    public function down()
    {
       return $this->dropTable('student');
    }
}
