<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m161225_124301_add_table_students_likes extends Migration
{
    public function up()
    {
        $this->createTable('student_like', [
            'id' => Schema::TYPE_PK,
            'student_like' => Schema::TYPE_INTEGER . ' NOT NULL',
            'student_liked' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable('student_like');
    }
}
