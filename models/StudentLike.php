<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "like".
 *
 * @property integer $id
 * @property integer $like
 * @property integer $liked
 * @property integer $created_at
 * @property integer $updated_at
 */
class StudentLike extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_like';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_like', 'student_liked', 'created_at', 'updated_at'], 'required'],
            [['student_like', 'student_liked', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_like' => 'Like',
            'student_liked' => 'Liked',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
